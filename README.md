# OpenML dataset: Marvel_Movies_Dataset

https://www.openml.org/d/46075

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The "Marvel Movies.csv" dataset is a comprehensive collection of financial and reception data related to Marvel Cinematic Universe (MCU) films. This dataset chronicles a selection of movies, providing insights into their financial success and critical reception. Covering films from the mid-2010s to the late 2010s, it includes various metrics such as yearly release information, worldwide gross, budget details, domestic and international earnings, opening and second weekend performance, and audience and critic scores. 

Attribute Description:
- **movie**: Title of the Marvel movie.
- **category**: Associated Marvel Comics character or franchise.
- **year**: Release year of the movie.
- **worldwide gross ($m)**: Total global box office earnings in millions of USD.
- **% budget recovered**: Percentage of the production budget recovered through earnings.
- **critics % score**: Percentage score awarded by critics.
- **audience % score**: Percentage score awarded by audiences.
- **audience vs critics % deviance**: Deviation in percentage points between audience and critic scores.
- **budget**: Production budget of the movie in millions of USD.
- **domestic gross ($m)**: Box office earnings in millions of USD within the domestic market.
- **international gross ($m)**: Box office earnings in millions of USD from international markets.
- **opening weekend ($m)**: Earnings in millions of USD during the opening weekend.
- **second weekend ($m)**: Earnings in millions of USD during the second weekend after release.
- **1st vs 2nd weekend drop off**: Percentage drop in earnings from the first to the second weekend.
- **% gross from opening weekend**: Percentage of total gross earnings acquired during the opening weekend.
- **% gross from domestic**: Percentage of total gross earnings from the domestic market.
- **% gross from international**: Percentage of total gross earnings from international markets.
- **% budget opening weekend**: Percentage of the production budget earned during the opening weekend.

Use Case:
This dataset is valuable for film industry analysts, marketing professionals, and business researchers focusing on the financial viability and reception of big-budget movies within major franchises. By examining metrics such as worldwide gross, audience versus critics score deviance, and budget recovery percentages, users can identify trends in film performance, audience preferences, and critical reception. Further, the dataset can aid in comparative studies across different MCU phases or characters, offering insights for future film projects and marketing strategies.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46075) of an [OpenML dataset](https://www.openml.org/d/46075). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46075/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46075/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46075/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

